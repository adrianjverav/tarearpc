#include <math.h>
#include "calculadora.h"

float *
addition_1_svc(input *argp, struct svc_req *rqstp)
{
	static float  result;
	operation op;	
	op.result = argp->a + argp->b;
	result = op.result;
	return &result;
}

float *
subtraction_1_svc(input *argp, struct svc_req *rqstp)
{
	static float  result;
	operation op;
	op.result = argp->a - argp->b;
	result = op.result;
	return &result;
}

float *
multiplication_1_svc(input *argp, struct svc_req *rqstp)
{
	static float  result;
	operation op;
	op.result = argp->a * argp->b;
	result = op.result;
	return &result;
}

float *
division_1_svc(input *argp, struct svc_req *rqstp)
{
	static float  result;
	operation op;
	op.result = argp->a / argp->b;
	result = op.result;
	return &result;
}

float *
pot_1_svc(input *argp, struct svc_req *rqstp)
{
	static float  result;
	operation op;
	op.result = pow(argp->a, argp->b);
	result = op.result;
	return &result;
}

float *
square_root_1_svc(input *argp, struct svc_req *rqstp)
{
	static float  result;
	operation op;
	op.result = sqrt(argp->a);
	result = op.result;
	return &result;
}