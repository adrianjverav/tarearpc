struct input {
	float a;
	float b;
};

struct operation {
	input in;
	char simbol[3];
	float result;
};

program CALCULATOR {
	version CALCULATOR_VR {
		float addition(input) = 1;
		float subtraction(input) = 2;
		float multiplication(input) = 3;
		float division(input) = 4;
	} = 1;
} = 0x30000001;