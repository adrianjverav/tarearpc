#ifndef _CALCULADORA_H_RPCGEN
#define _CALCULADORA_H_RPCGEN

#include <rpc/rpc.h>

#ifdef __cplusplus
extern "C" {
#endif


struct input {
	float a;
	float b;
};
typedef struct input input;

struct operation {
	input in;
	char simbol[3];
	float result;
};
typedef struct operation operation;

#define CALCULATOR 0x30000001
#define CALCULATOR_VR 1

#if defined(__STDC__) || defined(__cplusplus)
#define addition 1
extern  float * addition_1(input *, CLIENT *);
extern  float * addition_1_svc(input *, struct svc_req *);
#define subtraction 2
extern  float * subtraction_1(input *, CLIENT *);
extern  float * subtraction_1_svc(input *, struct svc_req *);
#define multiplication 3
extern  float * multiplication_1(input *, CLIENT *);
extern  float * multiplication_1_svc(input *, struct svc_req *);
#define division 4
extern  float * division_1(input *, CLIENT *);
extern  float * division_1_svc(input *, struct svc_req *);
#define pot 5
extern  float * pot_1(input *, CLIENT *);
extern  float * pot_1_svc(input *, struct svc_req *);
#define square_root 6
extern  float * square_root_1(input *, CLIENT *);
extern  float * square_root_1_svc(input *, struct svc_req *);
extern int calculator_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define addition 1
extern  float * addition_1();
extern  float * addition_1_svc();
#define subtraction 2
extern  float * subtraction_1();
extern  float * subtraction_1_svc();
#define multiplication 3
extern  float * multiplication_1();
extern  float * multiplication_1_svc();
#define division 4
extern  float * division_1();
extern  float * division_1_svc();
#define pot 5
extern  float * pot_1();
extern  float * pot_1_svc();
#define square_root 6
extern  float * square_root_1();
extern  float * square_root_1_svc();
extern int calculator_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */

#if defined(__STDC__) || defined(__cplusplus)
extern  bool_t xdr_input (XDR *, input*);
extern  bool_t xdr_operation (XDR *, operation*);

#else /* K&R C */
extern bool_t xdr_input ();
extern bool_t xdr_operation ();

#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_CALCULADORA_H_RPCGEN */
